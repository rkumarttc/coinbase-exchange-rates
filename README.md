# Coinbase Exchange Rates

#### Introduction

This is a single page Application (SPA) , which allows users to see the real time crypto rates. Frontend is written in React.js, Bootstrap-5 and Backend is using [Node.js](https://nodejs.org/dist/latest-v16.x/docs/api/), [ExpressJS](https://expressjs.com/en/5x/api.html) and [Socket.IO](https://socket.io/docs/v4/). Websocket used in thir applications allows us to communicate between server and the browser to share real time data.

### Project Structure

#### \_architecture

This directory contains project highlevel solution design diagrams.

#### client

This Contains our front-end layer. To run this locally follow below steps.

- Make sure you have node.js and npm is intalled on you local system.
- Open a terminal inside client directory and run `npm i` command.
- There is already `.env` file in the directory added temporarely where server url is defined.
- One `npm i` is completed. just run another command `npm start` this will create a server locally and open a browser tab.
- The frontend application will run on `https://localhost:3000`

#### server

This directory contains our backend application code. To run this locally follow below steps

- Open a terminal inside server directory and run `npm i`.
- Once this command is finished run `npm start`.
- This will create a localhost server and you can access it on `http://localhost:3001`

### Application UI

![app](assets/app.JPG)