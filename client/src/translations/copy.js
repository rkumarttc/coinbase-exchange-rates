const copy = {
  "exchangeRatesPage.table.heading.Name.text": "Name",
  "exchangeRatesPage.table.heading.exchangeRate.text": "Exchange Rate",
  "exchangeRatesPage.table.heading.price.text": "Price in USD($)",
  "exchangeRatesPage.table.heading.rate.text": "Rate Change",
};

export default copy;
