export const App = {
  BACKEND_URL: process.env.REACT_APP_BACKEND_URL,
};

export const NodeApi = {
  EXCHANGE_RATES_API: "/exchange-rates",
};

export const breakpoints = {
  XXL: 1920,
  XL: 1380,
  LG: 1024,
  MD: 768,
  SM: 476,
};
