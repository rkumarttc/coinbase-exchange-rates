/* eslint-disable no-undef */
/* eslint-disable no-return-assign */
import React from "react";
import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunkMiddleware from "redux-thunk";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { logger } from "redux-logger";
import { rootReducer } from "../store/rootReducer";

const middlewares = [thunkMiddleware, logger];
const middleware = applyMiddleware(...middlewares);
window.setImmediate = window.setTimeout;
export const renderWithRedux = (component, mockState) => {
  const store = createStore(
    rootReducer,
    mockState,
    composeWithDevTools(middleware)
  );
  const componentWithRedux = render(
    <Provider store={store}>{component}</Provider>
  );
  return { componentWithRedux, store };
};
