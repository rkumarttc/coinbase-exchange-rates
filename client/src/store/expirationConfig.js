/* eslint-disable import/no-anonymous-default-export */
import expireReducer from "redux-persist-expire";

export default (reducers) =>
  reducers.map((reducer) =>
    expireReducer(reducer?.key, {
      expireSeconds: reducer?.lifespan,
      autoExpire: true,
    })
  );
