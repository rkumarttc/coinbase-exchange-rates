import { GET_EXCHANGE_RATES } from "./exchangeRatesPage.types";

export const getExchangeRates = (payload) => ({
  type: GET_EXCHANGE_RATES,
  payload,
});
