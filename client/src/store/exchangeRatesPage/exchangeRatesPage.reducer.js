import produce from "immer";
import { GET_EXCHANGE_RATES } from "./exchangeRatesPage.types";

const initialState = {
  data: [],
};
export default function exchangeRatesPageReducer(state = initialState, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case GET_EXCHANGE_RATES:
        draft.data = action.payload;
        break;
      default:
        return state;
    }
  });
}
