import { getExchangeRates } from "./exchangeRatesPage.actions";

export const getExchangeRate = (data) => async (dispatch, getState) => {
  await dispatch(getExchangeRates(data));
};
