import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunkMiddleware from "redux-thunk";
import { persistStore } from "redux-persist";
import { logger } from "redux-logger";
import { isBrowser } from "../utils/renderUtils";
import { persistedReducer } from "./rootReducer";

let middlewares = [thunkMiddleware];

if (process.env.NODE_ENV === "development") {
  middlewares = [thunkMiddleware, logger];
}

const middleware = applyMiddleware(...middlewares);

const store = createStore(persistedReducer, composeWithDevTools(middleware));

const persistor = isBrowser ? persistStore(store) : undefined;

export { store, persistor };
