import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";
import configureExpiration from "./expirationConfig";
import exchangeRatesPageReducer from "./exchangeRatesPage/exchangeRatesPage.reducer";

export const persistedReducers = [{ key: "history", lifespan: 3600 * 24 * 90 }];

const rootConfig = {
  key: "root",
  version: 1,
  storage,
  transforms: configureExpiration(persistedReducers),
  stateReconciler: autoMergeLevel2,
  whitelist: persistedReducers.map((reducer) => reducer.key),
};

export const rootReducer = combineReducers({
  exchangeRatesPage: exchangeRatesPageReducer,
});

export const persistedReducer = persistReducer(rootConfig, rootReducer);
