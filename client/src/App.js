import React from "react";
import ExchangeRatesPage from "./components/Pages/ExchangeRatesPage";
import Header from "./components/organisms/Header/Header";
import Footer from "./components/organisms/Footer/Footer";
import "./App.scss";

function App() {
  return (
    <div className="App">
      <Header></Header>
      <div className="container-xxl position-relative p-0">
        <ExchangeRatesPage></ExchangeRatesPage>
      </div>
      <Footer></Footer>
    </div>
  );
}

export default App;
