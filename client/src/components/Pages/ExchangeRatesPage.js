/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect } from "react";
import GenericText from "../atoms/GenericText/GenericText";
import { useAppState, useAppDispatch } from "../../hooks/useStore";
import { getExchangeRate } from "../../store/exchangeRatesPage";
import Price from "../organisms/Price/Price";
import useInterval from "../../hooks/useInterval";
import Anchor from "../atoms/Anchor/Anchor";
import useSocket from "../../hooks/useSocket";
import copy from "../../translations/copy";

const ExchangeRatesPage = (props) => {
  const dispatch = useAppDispatch();
  const data = useAppState((state) => state.exchangeRatesPage.data);
  const socket = useSocket();

  /* Initial prices load */
  useEffect(() => {
    socket.emit("MESSAGE_SEND", new Date());
  }, []);

  /* Load prices every time a message is received from backend */
  useEffect(() => {
    const getExchangeRates = (data) => {
      dispatch(getExchangeRate(data));
    };
    socket.on("MESSAGE_SEND", getExchangeRates);
    return () => {
      socket.disconnect();
    };
  }, [data]);

  /* Send message to server every 15 seconds */
  useInterval(() => {
    socket.emit("MESSAGE_SEND", new Date());
  }, 5000);

  return (
    <div className="wrapper">
      <div className="TABS"></div>
      <div className="table-responsive">
        <table className="table text-nowrap">
          <thead className="table-light border">
            <tr className="shadow-lg p-3 mb-5 bg-white rounded">
              <th>{copy["exchangeRatesPage.table.heading.Name.text"]}</th>
              <th>
                {copy["exchangeRatesPage.table.heading.exchangeRate.text"]}
              </th>
              <th>{copy["exchangeRatesPage.table.heading.price.text"]}</th>
              <th>{copy["exchangeRatesPage.table.heading.rate.text"]}</th>
            </tr>
          </thead>
          <tbody className="shadow-sm p-3 mb-5 bg-white rounded">
            {data?.map((coin) => (
              <tr key={coin.symbol}>
                <td className="align-middle">
                  <div className="align-items-center">
                    <div className="ms-3 lh-1">
                      <h5 className=" mb-1">{coin.symbol}</h5>
                    </div>
                  </div>
                </td>
                <td className="align-middle">{coin.exchangeRate}</td>
                <td className="align-middle">{`$${coin.price}`}</td>
                <td className="align-middle">
                  <Price
                    receivedAmount={coin.price}
                    priceChange={coin.priceChange}
                  ></Price>
                </td>
                <td className="align-middle">
                  <div className="dropdown dropstart">
                    <a
                      className="text-muted text-primary-hover"
                      href="#"
                      role="button"
                      id="dropdownTeamOne"
                      data-bs-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-more-vertical icon-xxs"
                      >
                        <circle cx="12" cy="12" r="1"></circle>
                        <circle cx="12" cy="5" r="1"></circle>
                        <circle cx="12" cy="19" r="1"></circle>
                      </svg>
                    </a>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="dropdownTeamOne"
                    >
                      <Anchor
                        isAnchor
                        href="#"
                        className="dropdown-item"
                        children
                      >
                        <GenericText text="Action" />
                      </Anchor>
                      <Anchor
                        isAnchor
                        href="#"
                        className="dropdown-item"
                        children
                      >
                        <GenericText text="Action 2" />
                      </Anchor>
                      <Anchor
                        isAnchor
                        href="#"
                        className="dropdown-item"
                        children
                      >
                        <GenericText text="Action 3" />
                      </Anchor>
                    </div>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default ExchangeRatesPage;
