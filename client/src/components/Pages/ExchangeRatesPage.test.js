/* eslint-disable testing-library/prefer-screen-queries */
import React from "react";
import { renderWithRedux } from "../../utils/testUtils";
import ExchangeRatesPage from "./ExchangeRatesPage";

jest.mock("../atoms/GenericText/GenericText", () => () => (
  <div data-testid="GenericText" />
));

jest.mock("../organisms/Price/Price", () => () => <div data-testid="Price" />);

const mockState = {
  exchangeRatesPage: {
    data: [
      {
        symbol: "ALL",
        name: "Allion",
        exchangeRate: "$111.704258",
        price: "0.16",
        priceChange: "descreased-price",
      },
    ],
  },
};

describe("ExchangeRatesPage component", () => {
  test("should render", () => {
    const {
      componentWithRedux: { getByText, getByTestId, getAllByTestId },
    } = renderWithRedux(<ExchangeRatesPage />, mockState);
    expect(getByText("Name")).toBeInTheDocument();
    expect(getByText("Exchange Rate")).toBeInTheDocument();
    expect(getByText("Price in USD($)")).toBeInTheDocument();
    expect(getByText("Rate Change")).toBeInTheDocument();
    expect(getAllByTestId("GenericText")).toBeTruthy();
    expect(getByTestId("Price")).toBeInTheDocument();
  });
});
