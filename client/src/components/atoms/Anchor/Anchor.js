import React from "react";
import "./Anchor.scss";

const Anchor = ({ isAnchor, href, target, className, children }) =>
  isAnchor ? (
    <a
      className={`remove-link-styles ${className}`}
      href={href}
      target={target}
    >
      {children}
    </a>
  ) : (
    <>{children}</>
  );

export default Anchor;
