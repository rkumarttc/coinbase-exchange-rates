/* eslint-disable testing-library/prefer-screen-queries */
import React from "react";
import Anchor from "./Anchor";
import { renderWithRedux } from "../../../utils/testUtils";

test("renders Anchor component", () => {
  const {
    componentWithRedux: { getByTestId },
  } = renderWithRedux(
    <Anchor>
      <div data-testid="Anchor"></div>
    </Anchor>
  );
  expect(getByTestId("Anchor")).toBeInTheDocument();
});
