import React from "react";
import DOMPurify from "dompurify";
import classnames from "classnames";

const GenericText = (props) => {
  const { text, styles, className, onClick, restProps } = props;
  const decodedText = DOMPurify.sanitize(text);
  return (
    <p
      className={classnames(className)}
      onClick={onClick}
      style={styles}
      dangerouslySetInnerHTML={{
        __html: decodedText,
      }}
      {...restProps}
    ></p>
  );
};

export default GenericText;
