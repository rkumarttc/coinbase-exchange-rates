/* eslint-disable testing-library/prefer-screen-queries */
import React from "react";
import GenericText from "./GenericText";
import { renderWithRedux } from "../../../utils/testUtils";

jest.mock("dompurify", () => {
  return {
    sanitize: jest.fn().mockReturnValue("customText"),
  };
});

test("renders GenericText component", () => {
  const {
    componentWithRedux: { container },
  } = renderWithRedux(<GenericText text="customText" />);
  expect(container).toBeInTheDocument();
});
