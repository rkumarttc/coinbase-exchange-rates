/* eslint-disable testing-library/prefer-screen-queries */
import React from "react";
import Footer from "./Footer";
import { renderWithRedux } from "../../../utils/testUtils";

test("renders Price component", () => {
  const {
    componentWithRedux: { getByText },
  } = renderWithRedux(<Footer />);
  expect(getByText("Features")).toBeInTheDocument();
  expect(getByText("Cool stuff")).toBeInTheDocument();
});
