/* eslint-disable testing-library/prefer-screen-queries */
import React from "react";
import Header from "./Header";
import { renderWithRedux } from "../../../utils/testUtils";

test("renders Price component", () => {
  const {
    componentWithRedux: { getByText },
  } = renderWithRedux(<Header />);
  expect(getByText("Coinbase Exhange")).toBeInTheDocument();
});
