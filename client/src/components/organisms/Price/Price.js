import React, { useEffect } from "react";
import usePreviousValue from "../../../hooks/usePreviousValue";
import useCalculateDifference from "../../../hooks/useCalculateDifference";
import GenericText from "../../atoms/GenericText/GenericText";
import classnames from "classnames";
import "./Price.scss";

/* Using react.memo to cache component and stop rerendering
 when receivedAmount value is same as previous value */
const Price = React.memo((props) => {
  const value = (0).toFixed(2);
  const { receivedAmount } = props;
  const prevValue = usePreviousValue(receivedAmount);

  const diff = useCalculateDifference(receivedAmount, prevValue);
  useEffect(() => {}, [receivedAmount]);

  return (
    <div className="price">
      <div className="price__container">
        <div className="price__svg">
          {prevValue < receivedAmount && (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="green"
              className="bi bi-arrow-up-circle-fill"
              viewBox="0 0 16 16"
            >
              <path d="M16 8A8 8 0 1 0 0 8a8 8 0 0 0 16 0zm-7.5 3.5a.5.5 0 0 1-1 0V5.707L5.354 7.854a.5.5 0 1 1-.708-.708l3-3a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 5.707V11.5z" />
            </svg>
          )}
          {prevValue > receivedAmount && (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="red"
              className="bi bi-arrow-down-circle-fill"
              viewBox="0 0 16 16"
            >
              <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v5.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V4.5z" />
            </svg>
          )}
        </div>
        <div className="price__text">
          {/*    <GenericText text="$"></GenericText> */}
          <GenericText
            text={isNaN(diff) ? `${value}%` : `${diff.toFixed(2)}%`}
            className={classnames({
              price__increased: prevValue < receivedAmount,
              price__descreased: prevValue > receivedAmount,
            })}
          ></GenericText>
        </div>
      </div>
    </div>
  );
});

export default Price;
