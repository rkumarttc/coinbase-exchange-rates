/* eslint-disable testing-library/prefer-screen-queries */
import React from "react";
import Price from "./Price";
import { renderWithRedux } from "../../../utils/testUtils";

jest.mock("../../atoms/GenericText/GenericText", () => () => (
  <div data-testid="GenericText" />
));

test("renders Price component", () => {
  const {
    componentWithRedux: { getByTestId },
  } = renderWithRedux(<Price />);
  expect(getByTestId("GenericText")).toBeInTheDocument();
});
