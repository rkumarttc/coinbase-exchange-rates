/* eslint-disable testing-library/prefer-screen-queries */
import React from "react";
import App from "./App";
import { renderWithRedux } from "./utils/testUtils";

jest.mock("./components/Pages/ExchangeRatesPage", () => () => (
  <div data-testid="ExchangeRatesPage" />
));

test("renders app", () => {
  const {
    componentWithRedux: { getByTestId },
  } = renderWithRedux(<App />);
  expect(getByTestId("ExchangeRatesPage")).toBeInTheDocument();
});
