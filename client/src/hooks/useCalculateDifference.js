const useCalculateDifference = (num1, num2) => {
  return 100 * Math.abs((num1 - num2) / ((num1 + num2) / 2));
};

export default useCalculateDifference;
