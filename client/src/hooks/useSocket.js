import { io } from "socket.io-client";
import { App } from "../utils/constants";

const useSocket = () => io(App.BACKEND_URL);
export default useSocket;
