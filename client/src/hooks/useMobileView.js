import { useMediaQuery } from "react-responsive";
import { breakpoints } from "../utils/constants";

const useMobileView = () => useMediaQuery({ maxWidth: breakpoints.MD });
export default useMobileView;
