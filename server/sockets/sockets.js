const constants = require("../utils/constants");
const axios = require("axios").default;

module.exports = (socket) => {
  socket.on("disconnect", () => {
    socket.emit(constants.LOGOUT, {
      message: "User loggedout",
    });
  });

  socket.on(constants.MESSAGE_SEND, async () => {
    const response = await axios
      .get(`${constants.COIN_BASE_API}/v2/exchange-rates`)
      .catch((err) => err);

    const { data } = response;
    const { data: dataObj } = data;
    const { rates } = dataObj;

    const ratesList = await Object.entries(rates)
      .map(([key, value]) => ({
        symbol: key,
        exchangeRate: `$${value}`,
        price: Math.round((1 / value + Number.EPSILON) * 100) / 100,
      }))
      .sort((a, b) => b.price - a.price);

    socket.emit(constants.MESSAGE_SEND, ratesList);
  });
};
