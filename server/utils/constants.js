module.exports = {
  COIN_BASE_API: process.env.COINBASE_BASE_URL,
  MESSAGE_SEND: "MESSAGE_SEND",
  LOGOUT: "LOGOUT",
};
